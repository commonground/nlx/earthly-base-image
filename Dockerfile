FROM golang:1.20.6

# https://pkg.go.dev/golang.org/x/tools/cmd/goimports?tab=versions
ENV GOIMPORTS_VERSION=0.10.0

# https://github.com/protocolbuffers/protobuf/releases/latest
ENV PROTOBUF_VERSION=23.3

# https://github.com/grpc-ecosystem/grpc-gateway/releases
ENV GRPC_GATEWAY_VERSION=v2.16.0

# https://pkg.go.dev/google.golang.org/protobuf/cmd/protoc-gen-go?tab=versions
ENV PROTOC_GEN_GO_VERSION=1.31.0

# https://pkg.go.dev/google.golang.org/grpc/cmd/protoc-gen-go-grpc?tab=versions
ENV PROTOC_GEN_GO_GRPC_VERSION=1.3.0

# https://github.com/yoheimuta/protolint/releases
ENV PROTOC_GEN_PROTOLINT=0.45.0

# https://github.com/golang/mock/releases
ENV MOCKGEN_VERSION=1.6.0

# https://github.com/kyleconroy/sqlc/releases
ENV SQLC_VERSION=1.19.1

# https://github.com/dmarkham/enumer/tags
ENV ENUMER_VERSION=1.5.8

# https://github.com/vektra/mockery/tags
# added _INSTALL_ to env name because of https://github.com/vektra/mockery/issues/391
ENV MOCKERY_INSTALL_VERSION=2.30.16

# https://github.com/deepmap/oapi-codegen/tags
# https://gitlab.com/commonground/nlx/nlx/-/merge_requests/4082
ENV OAPI_CODEGEN_VERSION=1.12.4

ENV README_GENERATOR_HELM_VERSION=2.5.1

WORKDIR /src

 # General apk dependencies
RUN apt-get update && apt-get install unzip protobuf-compiler default-jdk nodejs npm -y

# General Go dependencies
RUN go install golang.org/x/tools/cmd/goimports@v${GOIMPORTS_VERSION}

# Proto dependencies

RUN curl -LO https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOBUF_VERSION}/protoc-${PROTOBUF_VERSION}-linux-x86_64.zip && \
    unzip -q -d /protobuf protoc-${PROTOBUF_VERSION}-linux-x86_64.zip 'include/*' && \
    rm protoc-${PROTOBUF_VERSION}-linux-x86_64.zip

RUN curl -LO https://github.com/googleapis/googleapis/archive/master.zip && \
    unzip -q master.zip 'googleapis-master/google/api/*' && \
    mv googleapis-master /protobuf/googleapis && \
    rm master.zip
    

RUN go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@${GRPC_GATEWAY_VERSION} && \
    go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@${GRPC_GATEWAY_VERSION} && \
    go install google.golang.org/protobuf/cmd/protoc-gen-go@v${PROTOC_GEN_GO_VERSION} && \
    go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v${PROTOC_GEN_GO_GRPC_VERSION} && \
    go install github.com/yoheimuta/protolint/cmd/protoc-gen-protolint@v${PROTOC_GEN_PROTOLINT}

# Mock dependencies // can be removed once Mockery is used everywhere, see nlx#1749
RUN go install github.com/golang/mock/mockgen@v${MOCKGEN_VERSION}

# Enum generator
RUN go install github.com/dmarkham/enumer@v${ENUMER_VERSION}

# OpenAPI code generator
RUN go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@v${OAPI_CODEGEN_VERSION}

# SQL generator
RUN curl -LO https://github.com/kyleconroy/sqlc/releases/download/v${SQLC_VERSION}/sqlc_${SQLC_VERSION}_linux_amd64.tar.gz && \
    tar xvf sqlc_${SQLC_VERSION}_linux_amd64.tar.gz && \
    mv sqlc /usr/bin/ && \
    rm sqlc_${SQLC_VERSION}_linux_amd64.tar.gz

# Mockery
RUN curl -LO https://github.com/vektra/mockery/releases/download/v${MOCKERY_INSTALL_VERSION}/mockery_${MOCKERY_INSTALL_VERSION}_Linux_x86_64.tar.gz && \
    tar xvf mockery_${MOCKERY_INSTALL_VERSION}_Linux_x86_64.tar.gz && \
    mv mockery /usr/bin/ && \
    rm mockery_${MOCKERY_INSTALL_VERSION}_Linux_x86_64.tar.gz

# Readme generator for Helm 
RUN curl -LO https://github.com/bitnami-labs/readme-generator-for-helm/archive/refs/tags/${README_GENERATOR_HELM_VERSION}.zip && \
    unzip -q ${README_GENERATOR_HELM_VERSION}.zip && \
    rm ${README_GENERATOR_HELM_VERSION}.zip && \
    npm install -g readme-generator-for-helm-${README_GENERATOR_HELM_VERSION}/ && \
    rm -Rf readme-generator-for-helm-${README_GENERATOR_HELM_VERSION}/
